# Simple Webnovel manager (WIP)

![screenshot](./Screenshot.png)

I stoped updating this sofware, for multiple reason:

+ python packaging is horrible, I probably spend more time trying to package
this for one linux distribution than developping it
+ I hate Quidiam now.
+ I was probably the only user xD

If you want to still read wuxiaworld offline, I wrote this bash script.
You can probably modify it for generating epub with pandoc :D

```
for i in {1..10}; do
    curl "https://www.wuxiaworld.com/novel/overgeared/og-chapter-$i" | \
    awk '/fr-view/{getline; print}' | \
    sed 's/^ *//g' | \
    pandoc -f html -t markdown | \
    sed '/-------/,$!d' | \
    pandoc -o "$i.pdf" --pdf-engine=xelatex;
done
```




## Feature:

+ GUI in pyqt
+ Dowload directly novel content from the original source
+ Free and open source
+ No ads

## Source available:

+ [Wuxiaworld](https://www.wuxiaworld.com)
+ [Quidiam](https://webnovel.com)

## Installing
### Arch linux

Aur package: [webnovel-manager-git](https://aur.archlinux.org/packages/webnovel-manager-git/)

### Other (Linux)
**Dependencies:** python3 python-pyqt5 python-requests python-fuzzywuzzy python-beautifulsoup4

We strongly recommend installing webnovelmanager somewhere in your home folder (~/.local is a good location) when installing per hand.
```
$ python setup.py install --root=~/.local
```
**Hint:** You may need to update your PATH and PYTHONPATH.
Put following in your .bashrc/.zshrc/... and restart your shell.
```
export PATH=$HOME/.local/usr/bin:${PATH}
export PYTHONPATH=$HOME/.local/usr/lib:${PYTHONPATH}
```

## Todo

+ GUI
    + Download chapter from the GUI
    + Keyboard shortkuts
+ Other
    + Fetch more info about a novel
    + A better cache system

## Contributing

For the moment I don't need lot of help. I trying to stabilize how I write source and implement the basic feature (download management, bookmark system).
But ideas are welcome :) and if you have experience in packaging for other linux distro and other OS, help his wellcome. 
I also need a better logo :)

## License

This project is GPL3 licensed

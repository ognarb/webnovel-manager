#! /usr/bin/python

# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

import sys
import site
import _thread
import time
import pprint

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtQml import qmlRegisterType, QQmlApplicationEngine

from webnovelmanager.config import Config
from webnovelmanager.sources.source import Source
from webnovelmanager.sources.sources import SourceList


def main():
    app = QGuiApplication(sys.argv)

    # register type for qml
    qmlRegisterType(Source, "Source", 1, 0, "Source")
    qmlRegisterType(SourceList, "Sources", 1, 0, "Sources")
    qmlRegisterType(Config, "Config", 1, 0, "Config")

    # Load source
    sourceList = SourceList()

    # Load config and set bookmark and favorite
    config = Config()
    load(sourceList, config) #TODO load this after the app start with qt slots

    mainWindow = QQmlApplicationEngine()

    # Bind source and config object with app.qml
    mainWindow.rootContext().setContextProperty('sourceList', sourceList)
    mainWindow.rootContext().setContextProperty('config', config)

    # Load app.qml
    mainWindow.load(site.getsitepackages()[0] + '/webnovelmanager/qml/main.qml')

    # exit app
    sys.exit(app.exec_())

def load(sourceList, config):
    sourceList.load()
    config.load()
    config.set_config(sourceList)

if __name__ == '__main__':
    main()

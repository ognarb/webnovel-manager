import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.3
import QtQuick.Window 2.4
import QtQuick.Controls 2.2 as QQC2
import org.kde.kirigami 2.4 as Kirigami

Kirigami.ApplicationWindow {
    signal loaded()
    id: root

    header: Kirigami.ToolBarApplicationHeader {}
    wideScreen: width > columnWidth * 3
    property int columnWidth: Kirigami.Units.gridUnit * 13
    property int footerHeight: Math.round(Kirigami.Units.gridUnit * 2.5)

    component.onCompleted: {
        root.loaded()
    }

    property var init1 : NovelsComponent {
        id: novelsComponent
        onSourcenameChanged: {
            chapterComponent.sourcename = sourcename
        }
        onNovelChanged: {
            chapterComponent.novel = novel
            chapterComponent.sourcename = sourcename
        }
    }
    property var init2 : ChapterComponent {
        id: chapterComponent
    }

    pageStack.defaultColumnWidth: columnWidth
    pageStack.initialPage: [init1, init2]


    Component {
        id: secondLayerComponent
        Kirigami.Page {
            title: "Settings"
            background: Rectangle {
                color: Kirigami.Theme.backgroundColor
            }
            footer: QQC2.ToolBar {
                height: root.footerHeight
                QQC2.ToolButton {
                    Layout.fillHeight: true
                    //make it square
                    implicitWidth: height
                    Kirigami.Icon {
                        anchors.centerIn: parent
                        width: Kirigami.Units.iconSizes.smallMedium
                        height: width
                        source: "configure"
                    }
                    onClicked: root.pageStack.layers.pop();
                }
            }
        }
    }
}

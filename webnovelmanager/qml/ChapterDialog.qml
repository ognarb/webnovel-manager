/*
This file is part of webnovel-manager.

webnovel-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

webnovel-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.7
import QtQuick.Controls 2.1

Dialog {
    id: dialog
    property int max
    signal finished(string chapterNumber)

    function chooseChapter(value, max) {
        form.valid.top = max
        form.chapterNumber.text = value
        dialog.open();
    }

    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2

    focus: true
    modal: true
    title: qsTr("")
    standardButtons: Dialog.Ok | Dialog.Cancel

    contentItem: ChapterForm {
        id: form
    }

    onAccepted: finished(form.chapterNumber.text)
}

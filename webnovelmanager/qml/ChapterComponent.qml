import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.3
import QtQuick.Window 2.4
import QtQuick.Controls 2.2 as QQC2
import org.kde.kirigami 2.4 as Kirigami

Kirigami.ScrollablePage {
    id: root
    property alias content: chapterContent.text
    property int chapterIndex: 0
    property int chapterMax: 0
    property var novel
    property var sourcename

    function update() {
        content = novel.chapters[chapterIndex].body
        title = "Chapter " + (chapterIndex + 1) + "(Progess "  +((chapterIndex + 1) / chapterMax * 100).toFixed(2) + "%)"
        if (sourcename != undefined) {
            config.reading_chapter(sourcename, novel.title, chapterIndex)
        }
        fav_button.favorite = novel.fav
    }

    Component.onCompleted: {
        let save = chapterIndex
        chapterIndex = 0
        chapterIndex = save
        update();
    }

    function chapterIndexDown() {
        if (chapterIndex > 0)
            chapterIndex--
    }

    function chapterIndexUp() {
        if (chapterIndex < chapterMax - 1)
            root.chapterIndex++
    }

    function chapterScrollDown() {
       root.flickable.ScrollBar.vertical.stepSize = 0.002;
       root.flickable.ScrollBar.vertical.decrease()
    }
        
    function chapterScrollUp() {
       root.flickable.ScrollBar.vertical.stepSize = 0.002;
       root.flickable.ScrollBar.vertical.increase()
    }

    onNovelChanged: {
        chapterIndex = novel.chapter
        chapterMax = novel.chapters.length
        update()
    }

    onChapterIndexChanged: {
        update()
    }

    ChapterDialog {
        id: chapterdialog
        max: chapterComponent.chapterMax
        onFinished: {
            chapterComponent.chapterIndex = parseInt(chapterNumber) - 1
        }
    }

    title: "Content"
    background: Rectangle {
        anchors.fill: parent
        color: Kirigami.Theme.backgroundColor
    }
    footer: QQC2.Control {
        height: footerHeight
        padding: Kirigami.Units.smallSpacing
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            Kirigami.Separator {
                Rectangle {
                    anchors.fill: parent
                    color: Kirigami.Theme.focusColor
                    visible: chatTextInput.activeFocus
                }
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                }
            }
        }
        contentItem: RowLayout {
            QQC2.ToolButton {
                Layout.fillHeight: true
                //make it square
                implicitWidth: height
                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: width
                    source: "go-previous"
                }
                onClicked: chapterIndexDown()
            }
            Text { // FIXME work but could be better
                Layout.fillWidth: true
                id: chatTextInput
            }
            QQC2.ToolButton {
                id: fav_button
                property bool favorite: false

                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: width
                    source: fav_button.favorite ? "emblem-favorite" : "rating-unrated"
                }
                onClicked: {
                    if (!fav_button.favorite)
                        config.fav(root.sourcename, root.novel.title)
                    else
                        config.unfav(root.sourcename, root.novel.title)

                    fav_button.favorite = !fav_button.favorite
                }
            }
            QQC2.ToolButton {
                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: width
                    source: "search"
                }
                onClicked: {
                    chapterdialog.chooseChapter(root.chapterIndex + 1, root.chapterMax)
                }
            }
            //NOTE: icon support in tool button in Qt 5.11
            QQC2.ToolButton {
                Layout.fillHeight: true
                //make it square
                implicitWidth: height
                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: width
                    source: "go-next"
                }
                onClicked: chapterIndexUp()
            }
        }
    }

    Text {
        id: chapterContent
        text: "hello"
        wrapMode: Text.Wrap
    }

    // Keyboard shortkuts
    Keys.onUpPressed: chapterScrollDown()
    Keys.onDownPressed: chapterScrollUp()
    Keys.onLeftPressed: chapterIndexDown()
    Keys.onRightPressed: chapterIndexUp()
}

import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.3
import QtQuick.Window 2.4
import QtQuick.Controls 2.2 as QQC2
import org.kde.kirigami 2.4 as Kirigami

Kirigami.ScrollablePage {
    id: root
    property alias sourcename: sourcelist.currentText
    property var novel


    title: "Novels"
    actions.main: Kirigami.Action {
        icon.name: "search"
        text: "Search"
    }
    background: Rectangle {
        anchors.fill: parent
        color: Kirigami.Theme.backgroundColor
    }
    footer: QQC2.ToolBar {
        height: root.footerHeight
        padding: Kirigami.Units.smallSpacing
        RowLayout {
            anchors.fill: parent
            spacing: Kirigami.Units.smallSpacing
            //NOTE: icon support in tool button in Qt 5.11
            QQC2.ToolButton {
                Layout.fillHeight: true
                //make it square
                implicitWidth: height
                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: width
                    source: "configure"
                }
                onClicked: root.pageStack.layers.push(secondLayerComponent);
            }
            QQC2.ComboBox {
                id: sourcelist
                textRole: "name"
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: sourceList.sources
                delegate: ItemDelegate {
                    id: sourceDelegate
                    property variant source: model
                    text: name
                }
            }
        }
    }
    ListView {
        id: novellist
        currentIndex: 2
        model: sourceList.sources[sourcelist.currentIndex].novels
        onCurrentIndexChanged: {
            root.novel = novellist.currentItem.novel
        }
        delegate: Kirigami.BasicListItem {
            id: novelDelegate
            property variant novel: model
            label: title
            //checkable: true
            checked: novellist.currentIndex == index
            separatorVisible: false
            reserveSpaceForIcon: false
            onClicked: {
                novellist.currentIndex = index
            }
        }
    }
    focus: true
}

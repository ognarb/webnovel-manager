/*
This file is part of webnovel-manager.

webnovel-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

webnovel-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

GridLayout {
    id: grid
    property alias chapterNumber: chapterNumber
    property alias valid: valid

    rows: 2
    columns: 1

    Label {
        text: qsTr("Choose chapter")
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
    }

    TextField {
        id: chapterNumber
        focus: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
        validator: IntValidator { id: valid; bottom:0; top: 0}
    }
}

# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import sys
import re
from PyQt5.QtQml import QQmlListProperty
from tornado import ioloop, httpclient
from slugify import slugify
from typing import List, Dict
import requests

from PyQt5.QtCore import pyqtProperty, QObject, pyqtSignal

class Source(QObject):
    """
    Source class representing one source where novel are fetched
    The source class is representing the way the novels available are fetched
    The source class should never be used directly and should be overwritten if you want to add a new source
    Naming convention for the class who overwrite this class is the name of the source with CamelCase
    """
    _novels = {}
    _name = None
    sourceChanged = pyqtSignal()
    json_encoder = None
    json_decoder = None

    path = None

    def __init__(self, parent=None):
        super().__init__(parent)

    @pyqtProperty(str, notify=sourceChanged)
    def name(self) -> str:
        return self._name

    @pyqtProperty(QQmlListProperty, notify=sourceChanged)
    def novels(self):
        _novels = []
        for novel in self._novels.values():
            _novels.append(novel)
        return QQmlListProperty(Novel, self, _novels)

    def load(self, config_path) -> None:
        """
        load a source from the cache
        if there is no cache throw an IOError
        :param config_path:
        :return:
        """
        cache = open(config_path + '/' + self.name, "r")
        data = cache.read()
        if len(data) > 0:
            self._novels = json.loads(data, object_hook=self.json_decoder)._novels
            if len(self._novels) == 0:
                # There should be no source with 0 novel => error somewhere :/
                print("No novels found on Harddisk, syncing...")
                raise IOError
        else:
            print("Cannot open file, syncing...")
            raise IOError
        cache.close()

    def sync(self) -> None:
        """
        load a source from internet
        :warning: need to be overwritten
        :return:
        """
        print("Override Me!")

    def save(self, config_path: str) -> None:
        """
        save a source to the cache
        """
        try:
            cache = open(config_path + '/' + self.name, "w")
        except IOError as e:
            print("Cannot open cache file. Error message: {0}" % e)
            raise Exception
        try:
            for novel in self._novels:
                print(novel)
            print(self.json_encoder.__name__ + ", novel type" + str(type(self)) + "self: " + str(dir(self)))
            j = json.dumps(self, cls=self.json_encoder)
            cache.write(j)
            cache.flush()
            cache.close()
        except Exception as e:
            print("Cannot write cache file. Error message: {0}" % e)
            raise Exception


class Novel(QObject):
    """Novel class reprensenting a novel and his chapter

    The novel class should never be used directly and should be overwritten if you want to add a new novel
    Naming convention for the class who overwrite this class is: <SourceName>Novel in CamelCase

    .. note:: Normally you should have only one type of novel per source
    """
    source = "default"
    novelChanged = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self._title = 'default'
        self.url = 'default'
        self._fav = False
        self._chapter = 0

    @pyqtProperty(str, notify=novelChanged)
    def title(self) -> str:
        return self._title

    @pyqtProperty(QQmlListProperty, notify=novelChanged)
    def chapters(self) -> QQmlListProperty:
        return QQmlListProperty(Chapter, self, self._chapters)

    @pyqtProperty(bool, notify=novelChanged)
    def fav(self) -> bool:
        return self._fav

    @fav.setter
    def fav(self, value: bool) -> bool:
        self._fav = value

    @pyqtProperty(int, notify=novelChanged)
    def chapter(self) -> int:
        return self._chapter

    @chapter.setter
    def chapter(self, value: str) -> None:
        self._chapter = value
        self.novelChanged.emit()
    
    def sync_message(self):
        print("Sync novel: " + self.title + ", chapters: " + str(len(self._chapters)) + ", source: " + self.source)

    def get_chapter_nth(self, index: int) -> 'Chapter':
        """
        Get the n-th chapter from this novel
        :raise ArrayOutOfBound
        """
        return self._chapters[index]

    def load_chapters(self) -> None:
        """
        Load the chapter from internet
        """
        raise NotImplementedError

    def __len__(self) -> int:
        return len(self._chapters)

    def handle_request(self, response):
        print(response.request.url)
        global i
        self.i -= 1
        if self.i == 0:
            ioloop.IOLoop.instance().stop()
        index = self.index_download
        self.index_download += 1
        chapterfile = open(os.path.expanduser(
            '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(self._title) + '/' + str(
                index)), 'w')
        chapterfile.write(self._chapters[index].get_body(response.body))

    i = 0
    index_download = 0

    def download(self, interval):
        """
        Download a specific interval of chapter
        The chapter interval need to be given in the form '\d+..\d*'
        Default interval is '1..' (first to last chapter)
        """
        result = re.match('(\d+)..(\d*)', interval)
        (s, e) = result.group(1, 2)
        if e == '':
            e = -1
        (start, end) = (int(s), int(e))
        if start > end != -1:
            print('Start of interval bigger as end')
            sys.exit(1)
        if end > len(self):
            print('Interval bigger as number as chapter')
            sys.exit(1)

        print("Download from " + str(start) + ' to ' + str(end))

        if not os.path.exists(os.path.expanduser('~/.local/share/webnovel-manager/' + self.source + '-content')):
            os.makedirs(os.path.expanduser(
                '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(self._title)))

        if not os.path.exists(os.path.expanduser(
                '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(self._title))):
            os.makedirs(os.path.expanduser(
                '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(self._title)))

        http_client = httpclient.AsyncHTTPClient()
        urls = []
        for chapter in self.chapters:
            if chapter.index >= start and (chapter.index <= end or end == -1):
                urls.append(chapter.url)

        self.index_download = start
        for url in urls:
            self.i += 1
            http_client.fetch(url.strip(), self.handle_request, method='GET')
            ioloop.IOLoop.instance().start()


class Chapter(QObject):
    """
    Chapter class representing a chapter from a novel
    The chapter class should never be used directly and should be overwrite if you want to add a new chapter
    Hint: Normally you shoud only have one type of chapter per novel
    Naming convention for the class who overwrite this class is: <SourceName>Chapter in CamelCase
    """

    chapterChanged = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.url = 'default'
        self.local = None
        self._index = -1

    @staticmethod
    def replace_with_newlines(element):
        text = ''
        for elem in element.descendants:
            if isinstance(elem, str):
                text += elem.strip()
            elif elem.name == 'br':
                text += '\n' * 2
            elif elem.name == 'p':
                text += '\n' * 2
        return text

    @staticmethod
    def parse_html(soup):
        """
        Default html converter
        convert html to text with only new line
        TODO: make this function even better and maybe add markdown suport :D
        source: https://stackoverflow.com/a/33196958
        """
        plain_text = ''
        return Chapter.replace_with_newlines(soup)
        # for line in soup.findAll('p'):
        #    print(line)
        #    line = Chapter.replace_with_newlines(line)
        #    plain_text+=line + "\n\n"
        # return plain_text

    @pyqtProperty(int, notify=chapterChanged)
    def index(self):
        return self._index

    @pyqtProperty(str, notify=chapterChanged)
    def body(self):
        try:
            if self.local == None:
                raise Exception

            contentF = open(os.path.expanduser(self.local), 'r')
            body = contentF.read()
            contentF.close()
            return body

        except Exception as e:
            try:
                # not available locally, download it
                r = requests.get(self.url)
                r.encoding = 'utf-8'
                b = self.get_body(r.text)
                # save it on the hard disk
                dirName = os.path.expanduser(re.sub('/[^/]*$', '/', self.local))
                os.makedirs(dirName, exist_ok=True)
                chapterfile = open(os.path.expanduser(self.local), 'w')
                chapterfile.write(b)
                return b
            except Exception as e:
                return 'No Internet Connection :/'

    def get_body(self, content):
        """
        From the given url, should return a text object with the chapter body with no html code and readable (with new line and tab)
        Hint: you can use parse_html to help you and maybe even make parse_html better
        """
        raise NotImplementedError


if __name__ == "__main__":
    print("don't run this script directly")

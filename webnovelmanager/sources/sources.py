# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import subprocess

from PyQt5.QtCore import QObject, pyqtProperty, pyqtSignal
from PyQt5.QtQml import QQmlListProperty

from webnovelmanager.sources.quidiam import Quidiam
from webnovelmanager.sources.source import Source
from webnovelmanager.sources.wuxiaworld import Wuxiaworld
from webnovelmanager.sources.fav import Favorite


class SourceList(QObject):
    # pyqtSignal
    sourcesChanged = pyqtSignal()
    finishLoading = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.config_path = os.path.expanduser('~/.local/share/webnovel-manager')
        self._sources = [Favorite(), Wuxiaworld(), Quidiam()]
        self.fav = self._sources[0]

    def sync_and_save(self):
        """
        Force sync all metadata
        :return:
        """
        if not os.path.exists(self.config_path):
            os.makedirs(self.config_path)

        for source in self._sources:
            try:
                print("Sync " + source.name)
                source.sync()
                print("Save " + source.name)
                source.save(self.config_path)
                self.sourcesChanged.emit()
            except Exception as e:
                subprocess.Popen(['notify-send', "unable to sync " + source.name])
                print("Error in sync: {0}" % e)
                source._novels = {}

    def load(self):
        """
        Load all sources from cache
        :return:
        """
        if not os.path.exists(self.config_path):
            os.makedirs(self.config_path)

        for source in self._sources:
            try:
                source.load(self.config_path)
                self.sourcesChanged.emit()
            except IOError:
                source.sync()

    @pyqtProperty(QQmlListProperty, notify=sourcesChanged)
    def sources(self):
        return QQmlListProperty(Source, self, self._sources)

    @sources.setter
    def sources(self, sources):
        if sources != self._sources:
            self._sources = sources
            self.sourcesChanged.emit()

# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

from webnovelmanager.sources.source import Source, Novel, Chapter
import json, requests
from bs4 import BeautifulSoup
from slugify import slugify


def get_quidiam_urls(limit=1):
    return ["https://www.webnovel.com/apiajax/category/ajax?orderBy=4&pageIndex={0}&category=0".format(i + 1) for i in
            range(limit)]

class json_encoder_quidiam(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Quidiam):
            return {"__Quidiam__": True, "novels": obj._novels}
        if isinstance(obj, QuidiamNovel):
            return {"__QuidiamNovel__": True, "title": obj._title, "url": obj.url, "chapters": obj._chapters, "bookId": obj.bookId}
        if isinstance(obj, QuidiamChapter):
            # for backwards compability
            try:
                location = obj.local
            except Exception as e:
                location = None
            return {'__QuidiamChapter__': True, 'local': location, "index": obj._index, "url": obj.url}
        return json.JSONEncoder.default(self, obj)


def json_decoder_quidiam(dct):
    if '__Quidiam__' in dct:
        q = Quidiam()
        q._novels = dct['novels']
        return q
    if '__QuidiamNovel__' in dct:
        q = QuidiamNovel()
        q._chapters = dct['chapters']
        q.bookId = dct['bookId']
        q.url = dct['url']
        q._title = dct['title']
        return q
    if '__QuidiamChapter__' in dct:
        q = QuidiamChapter()
        # for backwards Compability
        try:
            q.local = dct['local']
        except Exception as e:
            q.local = None
        q._index = dct['index']
        q.url = dct['url']
        return q
    return dct


class Quidiam(Source):
    _name = "quidiam"
    _novels = {}

    def __init__(self, parent=None):
        super().__init__(parent)
        self.json_encoder = json_encoder_quidiam
        self.json_decoder = json_decoder_quidiam

    def sync(self):
        for url in get_quidiam_urls():
            r = requests.get(url)
            r.encoding = "utf-8"
            novels = json.loads(r.text)["data"]["items"]
            for novel in novels:
                n = QuidiamNovel()
                n.url = "https://https://www.webnovel.com/book/" + novel["bookId"]
                n._title = novel["bookName"]
                n.bookId = novel["bookId"]
                n.load_chapters()
                self._novels[n._title] = n
                n.sync_message()
        for novel in self._novels.values():
            print(novel._title)

class QuidiamNovel(Novel):
    source = "quidiam"
    def __init__(self, parent=None):
        super(QuidiamNovel, self).__init__(parent)

    def load_chapters(self):
        try:
            r = requests.get("https://www.webnovel.com/apiajax/chapter/GetChapterList?bookId=" + self.bookId)
        except Exception as e:
            try:
                r = requests.get("https://www.webnovel.com/apiajax/chapter/GetChapterList?bookId=" + self.bookId)
            except Exception as e:
                raise Exception("Connection error")
        r.encoding = 'utf-8'
        apiData = json.loads(r.text)
        chapters = []
        for volume in apiData["data"]["volumeItems"]:
            chapters += volume["chapterItems"]
        i = 0
        self._chapters = []
        for chapter in chapters:
            q = QuidiamChapter()
            q._index = i
            q.local = '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(
                self._title) + '/' + str(i)
            q.url = "https://www.webnovel.com/apiajax/chapter/GetContent?bookId={0}&chapterId={1}".format(self.bookId,
                                                                                                          chapter["id"])
            self._chapters.append(q)
            i += 1


class QuidiamChapter(Chapter):
    def get_body(self, content):
        json_file = json.loads(content)
        if (json_file['data']['chapterInfo']['isRichFormat'] == 1):
            ret = self.parse_html(BeautifulSoup(json_file['data']['chapterInfo']['content'], 'html.parser'))
        else:
            ret = json.loads(content)["data"]["chapterInfo"]["content"]
        return ret


if __name__ == "__main__":
    pass

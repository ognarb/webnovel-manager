# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

from webnovelmanager.sources.source import Source


class Favorite(Source):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._name = 'Favorite'

    def sync(self):
        pass

    def load(self, config_path):
        pass

    def save(self, config_path):
        pass

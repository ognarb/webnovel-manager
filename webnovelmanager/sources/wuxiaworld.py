# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import requests
from bs4 import BeautifulSoup
from slugify import slugify

from webnovelmanager.sources.source import Source, Novel, Chapter

ALL_OF_WUXIA = ["/tag/completed", "/language/chinese", "/language/korean", "/language/english"]

class json_encoder_wuxiaworld(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Wuxiaworld):
            return {"__Wuxiaworld__": True, "novels": obj._novels}
        if isinstance(obj, WuxiaworldNovel):
            return {"__WuxiaworldNovel__": True, "title": obj._title, "url": obj.url, "chapters": obj._chapters}
        if isinstance(obj, WuxiaworldChapter):
            # for backwards compability
            try:
                location = obj.local
            except Exception as e:
                location = None
            return {'__WuxiaworldChapter__': True, "index": obj._index, "url": obj.url, 'local': location}
        return json.JSONEncoder.default(self, obj)

def json_decoder_wuxiaworld(dct):
    if '__Wuxiaworld__' in dct:
        w = Wuxiaworld()
        w._novels = dct['novels']
        return w
    if '__WuxiaworldNovel__' in dct:
        w = WuxiaworldNovel()
        w._title = dct['title']
        w.url = dct['url']
        w._chapters = dct['chapters']
        return w
    if '__WuxiaworldChapter__' in dct:
        w = WuxiaworldChapter()
        # for backwards Compability
        try:
            w.local = dct['local']
        except Exception as e:
            w.local = None
        w._index = dct['index']
        w.url = dct['url']
        return w
    return dct

class Wuxiaworld(Source):
    _name = "wuxiaworld"

    def __init__(self, parent=None):
        super().__init__(parent)
        self.json_decoder = json_decoder_wuxiaworld
        self.json_encoder = json_encoder_wuxiaworld

    def sync(self, subUrl=ALL_OF_WUXIA):
        medias = []
        for url in subUrl:
            r = requests.get('https://www.wuxiaworld.com' + url)
            r.encoding = "urf-8"
            soup = BeautifulSoup(r.text, "html.parser")
            medias += soup.find_all(class_="media")

            for media in medias:
                url = media.find("a")
                title = media.find("h4")
                if title is not None:
                    novel = WuxiaworldNovel()
                    novel._title = str(title.contents[0])
                    novel.url = url["href"]
                    novel.load_chapters()
                    self._novels[novel.title] = novel
                    novel.sync_message()
        for novel in self._novels:
            print(novel.title)


class WuxiaworldNovel(Novel):
    source = 'wuxiaworld'

    def load_chapters(self):
        r = requests.get("https://www.wuxiaworld.com" + self.url)
        r.encoding = "utf-8"
        soup = BeautifulSoup(r.text, "html.parser")
        chapters = soup.find_all(class_="chapter-item")
        i = 0
        self._chapters = []
        for chapter in chapters:
            w = WuxiaworldChapter()
            w._index = i
            w.local = '~/.local/share/webnovel-manager/' + self.source + '-content/' + slugify(
                self._title) + '/' + str(i)
            w.url = "https://www.wuxiaworld.com" + chapter.a['href']
            self._chapters.append(w)
            i += 1


class WuxiaworldChapter(Chapter):
    def get_body(self, content):
        soup = BeautifulSoup(content, "html.parser")
        htmlraw = soup.find(class_="fr-view")
        return self.parse_html(htmlraw)


if __name__ == "__main__":
    pass

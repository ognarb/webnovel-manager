# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

import os

import toml
from PyQt5.QtCore import QObject, pyqtSlot

from webnovelmanager.sources.fav import Favorite

class Config(QObject):
    """
    Class responsible to handle the config
    can add/remove a novel to favorite
    can bookmark reading progress
    """
    config = {
        'sources': {
            'loaded': []
        },
        'novels': [],
        'reading': {},
        'fav': {}
    }

    def __init__(self, parent=None):
        super().__init__(parent)
        self.save_path = os.path.expanduser('~/.config/webnovel-manager')

    def get_novel(self, sourcename, novelname):
        """
        Get a novel from his name and the name of the source
        :param sourcename:
        :param novelname:
        :return:
        """
        for source in self.sources._sources:
            if source.name == sourcename and novelname in source._novels:
                return source._novels[novelname]
        return None

    @pyqtSlot(str, str)
    def fav(self, sourcename, novelname):
        """
        Add a novel to the favorite
        :param sourcename:
        :param novelname:
        :return:
        """
        if 'fav' not in self.config:
            self.config['fav'] = {}
        if sourcename not in self.config['fav']:
            self.config['fav'][sourcename] = []
            self.config['fav'][sourcename].append(novelname)
        else:
            self.config['fav'][sourcename].append(novelname)
        novel = self.get_novel(sourcename, novelname)
        if novel is None:
            print('error')
            return
        self.sources.fav._novels[novelname] = novel
        novel.fav = True
        self.save()

    @pyqtSlot(str, str)
    def unfav(self, source_name, novel_name):
        """
        Remove a novel from the favorite
        :param source_name:
        :param novel_name:
        :return:
        """
        if 'fav' not in self.config:
            self.config['fav'] = {}
        if source_name in self.config['fav']:
            if novel_name in self.config['fav'][source_name]:
                self.config['fav'][source_name].remove(novel_name)
        novel = self.get_novel(source_name, novel_name)
        if novel is None:
            print('error')
            return
        novel.fav = False
        self.sources.fav._novels.pop(novel_name)
        self.save()

    @pyqtSlot(str, str, int)
    def reading_chapter(self, source_name, novel_name, chapter):
        """
        Save the progress for a specific novel
        :param source_name:
        :param novel_name:
        :param chapter:
        :return:
        """
        # print("updating reading progress: {} {} {}".format(sourcename, novelname, chapter))
        if 'reading' not in self.config:
            self.config['reading'] = {}
        if source_name not in self.config['reading']:
            self.config['reading'][source_name] = {}
            self.config['reading'][source_name][novel_name] = chapter
        else:
            self.config['reading'][source_name][novel_name] = chapter

        novel = self.get_novel(source_name, novel_name)
        if novel is None:
            print('error the novel ' + novel_name + ' in ' + source_name + ' don\'t exist in the database')
            return
        novel.chapter = chapter
        self.save()

    def set_config(self, sources):
        """
        Init the config class with sources
        :param sources:
        :return:
        """
        self.sources = sources

        if 'fav' not in self.config:
            self.config['fav'] = {}

        if 'reading' not in self.config:
            self.config['reading'] = {}

        for source in self.sources._sources:
            if source.name in self.config['fav'] and source.name != 'Favorite':
                sourcefav = self.config['fav'][source.name]
                # a least one novel from source is in favorite (if array is not empty)
                for novelname, novel in source._novels.items():
                    if novelname in sourcefav:
                        novel._fav = True
                        sources.fav._novels[novelname] = novel
            if source.name in self.config['reading'] and source.name != 'Favorite':
                sourceread = self.config['reading'][source.name]
                for novelname, novel in source._novels.items():
                    if novelname in sourceread:
                        novel.chapter = sourceread[novelname]
                        # print(novel._chapter)

    def save(self):
        """
        save the config to filesystem
        :return:
        """
        if not os.path.exists(self.save_path):
            os.makedirs(self.save_path)
            print('Create a new config file')
        try:
            config_file = open(self.save_path + '/webnovel_manager_config', "w")
        except Exception as e:
            print("{0}".format(e))
            raise RuntimeError

        toml.dump(self.config, config_file)

    def load(self):
        """
        load the config from filesystem
        :return:
        """
        if not os.path.exists(self.save_path + '/webnovel_manager_config'):
            print('No file or directory at \'' + self.save_path + '\'')
            self.save()
            return

        try:
            config_file = open(self.save_path + '/webnovel_manager_config', "r")
        except Exception as e:
            print("{0}".format(e))
            raise RuntimeError

        self.config = toml.load(config_file)

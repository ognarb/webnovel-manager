#! /bin/python
# This file is part of webnovel-manager.

# webnovel-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# webnovel-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with webnovel-manager.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages
import os

qml_package_data = list()
qml_dir = os.path.abspath('webnovelmanager/qml')
for root, dirs, files in os.walk(qml_dir):
    for suffix in ("ttf", "qml", "js", "txt", "png", "py", "otf"):
        relpath = os.path.relpath(root, qml_dir)
        relpath = relpath.replace("\\", "/")
        qml_package_data.append("qml/" + relpath.strip(".") + "/*." + suffix)

setup(
    name="webnovelmanager",
    version="0.1",
    packages=find_packages(),
    package_data={
        "webnovelmanager": qml_package_data
    },
    license="GPL3",
    url="https://github.com/ognarb/webnovel-manager",
    scripts=['scripts/webnovel-manager-cli', 'scripts/webnovel-manager']
)
